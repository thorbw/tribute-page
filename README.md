

# freeCodeCamp project: tribute page with Ian Murdock 


This project is only a web page and here is the link:

[tribute page Ian Murdock](https://thorbw.gitlab.io/tribute-page/)


## User Story: 

* I can view a tribute page with an image and text.
* I can click on a link that will take me to an external website with further information on the topic.


